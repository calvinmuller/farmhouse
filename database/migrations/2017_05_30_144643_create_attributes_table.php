<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('attributes', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('brand_id')->unsigned();
      $table->string('renderer')->nullable();
      $table->string('frontend_input');
      $table->string('backend_type');
      $table->string('attribute_code');
      $table->string('attribute_name');
      $table->string('attribute_label');
      $table->boolean('is_configurable')->default(1);
      $table->timestamps();

      $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('attributes');
  }
}

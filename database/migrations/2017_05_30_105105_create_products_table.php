<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('products', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('brand_id');
      $table->integer('attribute_set_id');
      $table->string('sku');
      $table->string('name');
      $table->string('brand');
      $table->decimal('price');
      $table->decimal('cost')->nullable();
      $table->decimal('special_price')->nullable();
      $table->dateTime('special_from')->nullable();
      $table->dateTime('special_to')->nullable();
      $table->boolean('is_parent')->default(false);
      $table->boolean('has_options')->default(false);
      $table->integer('parent_id')->default(0);
      $table->integer('visibility')->default(1);
      $table->string('type_id')->nullable();
      $table->integer('supplier_id')->nullable();
      $table->timestamps();

//
//      $table->foreign('attribute_set_id')->references('id')->on('attribute_sets')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('products');
  }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {

    Schema::create('brands', function (Blueprint $table) {
      $table->increments('id');
      $table->uuid('uuid')->nullable();
      $table->string('name');
      $table->string('address')->nullable();
      $table->string('telephone')->nullable();
      $table->timestamps();
    });

    DB::unprepared('
        CREATE TRIGGER uuid_default BEFORE INSERT ON `brands` FOR EACH ROW
            BEGIN
              SET NEW.`uuid` = UUID();
            END
        ');
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('brands');
//    Schema::dropIfExists('uuid_default');
  }
}

<?php

use Illuminate\Database\Seeder;

class AttributeOptionsSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    //

    $attribute = \App\Attribute::loadByCode('colour');

    $attribute->options()->createMany([
      [
        'value' => 'Red'
      ], [
        'value' => 'Green'
      ], [
        'value' => 'Duck Green'
      ], [
        'value' => 'Blue'
      ], [
        'value' => 'Navy Blue'
      ], [
        'value' => 'Beige'
      ], [
        'value' => 'Burgundy'
      ]
    ]);

    $attribute = \App\Attribute::loadByCode('size');

    $attribute->options()->createMany([
      [
        'value' => 'XS'
      ], [
        'value' => 'S'
      ], [
        'value' => 'M'
      ], [
        'value' => 'L'
      ], [
        'value' => 'XL'
      ]
    ]);

  }
}

<?php

use Illuminate\Database\Seeder;

class AttributeSetSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    //

    $attributeSet = \App\AttributeSet::create([
      'name' => 'Default'
    ]);

    $attributes = [
      [
        'attribute_code' => 'colour',
        'attribute_label' => 'Colour',
        'attribute_name' => 'Colour',
        'is_configurable' => 1,
        'brand_id' => 1,
        'frontend_input' => 'select',
        'backend_type' => 'int'
      ],
      [
        'attribute_code' => 'size',
        'attribute_label' => 'Size',
        'attribute_name' => 'Size',
        'is_configurable' => 1,
        'brand_id' => 1,
        'frontend_input' => 'select',
        'backend_type' => 'int'
      ]
    ];

    $attributeSet->attributes()->createMany($attributes);
  }
}

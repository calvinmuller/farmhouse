task :composer_install do
  on roles(:web) do
    within release_path do
      execute :composer, "install", "--no-dev"
    end
  end
end

task :cache_clear do
  on roles(:web) do
    within release_path do
      execute :php, "artisan", "api:cache"
      execute :php, "artisan", "cache:clear"
      execute :php, "artisan", "config:cache"
    end
  end
end

task :migrate do
  on roles(:web) do
    within release_path do
      execute :php, "artisan", "migrate", "--force"
    end
  end
end

task :restart_php do
  on roles(:web) do
    within release_path do
      execute :sudo, "service", "php-fpm-7.1", "reload"
    end
  end
end

task :clear_routes_cache do
  on roles(:web) do
    within release_path do
      execute :php, "artisan", "api:cache"
    end
  end
end

task :chown do
  on roles(:web) do
    within release_path do
      execute :chown, "-R", "api:api", "*"
    end
  end
end

before "deploy:symlink:release", "composer_install"
after "composer_install", "migrate"
after "migrate", "cache_clear"
after "cache_clear", "restart_php"
after "restart_php", "clear_routes_cache"
after "clear_routes_cache", "chown"


@component('mail::brandmessage', ['brand' => $brand])

# Hi {{ $brand->name }}

You have had some interest on your website from:

@component('mail::table')
| Name       | Email    |   Telephone
| ------------- |:-------------:|
| {{$data['name']}}     | {{$data['email']}}      | {{$data['telephone']}}
@endcomponent

@component('mail::panel')
{{$data['message']}}
@endcomponent

Thanks,<br>
{{ $brand->name }}
@endcomponent

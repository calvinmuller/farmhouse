---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)
<!-- END_INFO -->

#Attribute Sets
Class AttributeSetController
<!-- START_f9a4fa37996c30e5362423d9b931dc26 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://farmhouse.dev//admin/attribute-sets" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/attribute-sets",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /admin/attribute-sets`

`HEAD /admin/attribute-sets`


<!-- END_f9a4fa37996c30e5362423d9b931dc26 -->

<!-- START_569268dae95884ea4ccd6f08a77cfc2b -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://farmhouse.dev//admin/attribute-sets" \
-H "Accept: application/json" \
    -d "name"="et" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/attribute-sets",
    "method": "POST",
    "data": {
        "name": "et"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /admin/attribute-sets`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | 

<!-- END_569268dae95884ea4ccd6f08a77cfc2b -->

<!-- START_2c0a107f1235292a9f9d584cb5b3bd53 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://farmhouse.dev//admin/attribute-sets/{attribute_set}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/attribute-sets/{attribute_set}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /admin/attribute-sets/{attribute_set}`

`HEAD /admin/attribute-sets/{attribute_set}`


<!-- END_2c0a107f1235292a9f9d584cb5b3bd53 -->

<!-- START_572ecb0c096de6315af9e9c3bb5048a4 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://farmhouse.dev//admin/attribute-sets/{attribute_set}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/attribute-sets/{attribute_set}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT /admin/attribute-sets/{attribute_set}`

`PATCH /admin/attribute-sets/{attribute_set}`


<!-- END_572ecb0c096de6315af9e9c3bb5048a4 -->

<!-- START_abc5c136639c6375b021d0f048cb0820 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://farmhouse.dev//admin/attribute-sets/{attribute_set}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/attribute-sets/{attribute_set}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE /admin/attribute-sets/{attribute_set}`


<!-- END_abc5c136639c6375b021d0f048cb0820 -->

#Attributes
Class AttributeController
<!-- START_f480e763b22d57bf7cd4e9a01e50ca01 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://farmhouse.dev//admin/attributes" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/attributes",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /admin/attributes`

`HEAD /admin/attributes`


<!-- END_f480e763b22d57bf7cd4e9a01e50ca01 -->

<!-- START_abb1370c0b3a19d905cead378479ce9c -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://farmhouse.dev//admin/attributes" \
-H "Accept: application/json" \
    -d "renderer"="reprehenderit" \
    -d "frontend_input"="reprehenderit" \
    -d "backend_type"="reprehenderit" \
    -d "attribute_code"="reprehenderit" \
    -d "attribute_name"="reprehenderit" \
    -d "attribute_label"="reprehenderit" \
    -d "is_configurable"="1" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/attributes",
    "method": "POST",
    "data": {
        "renderer": "reprehenderit",
        "frontend_input": "reprehenderit",
        "backend_type": "reprehenderit",
        "attribute_code": "reprehenderit",
        "attribute_name": "reprehenderit",
        "attribute_label": "reprehenderit",
        "is_configurable": true
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /admin/attributes`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    renderer | string |  optional  | 
    frontend_input | string |  required  | 
    backend_type | string |  required  | 
    attribute_code | string |  required  | 
    attribute_name | string |  required  | 
    attribute_label | string |  required  | 
    is_configurable | boolean |  required  | 

<!-- END_abb1370c0b3a19d905cead378479ce9c -->

<!-- START_a204c03da97666361f8ed2037a781fb0 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://farmhouse.dev//admin/attributes/{attribute}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/attributes/{attribute}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /admin/attributes/{attribute}`

`HEAD /admin/attributes/{attribute}`


<!-- END_a204c03da97666361f8ed2037a781fb0 -->

<!-- START_1c0750cfcd5893949d1f343727164d78 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://farmhouse.dev//admin/attributes/{attribute}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/attributes/{attribute}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT /admin/attributes/{attribute}`

`PATCH /admin/attributes/{attribute}`


<!-- END_1c0750cfcd5893949d1f343727164d78 -->

<!-- START_110c0c703012b07c978aaa0f1dbb6049 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://farmhouse.dev//admin/attributes/{attribute}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/attributes/{attribute}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE /admin/attributes/{attribute}`


<!-- END_110c0c703012b07c978aaa0f1dbb6049 -->

#Auth
Class AuthenticateController
<!-- START_ac6527c96d4b9793a4c77ff1e22a8906 -->
## API Login, on success return JWT Auth token

> Example request:

```bash
curl -X POST "http://farmhouse.dev//auth/login" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//auth/login",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /auth/login`


<!-- END_ac6527c96d4b9793a4c77ff1e22a8906 -->

<!-- START_d933f1192afaa6a7e0691a8fc5e68642 -->
## Log out
Invalidate the token, so user cannot use it anymore
They have to relogin to get a new token

> Example request:

```bash
curl -X POST "http://farmhouse.dev//auth/logout" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//auth/logout",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /auth/logout`


<!-- END_d933f1192afaa6a7e0691a8fc5e68642 -->

<!-- START_2befb2b1824b6f992a137039911e1a3d -->
## Refresh the token

> Example request:

```bash
curl -X GET "http://farmhouse.dev//auth/token" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//auth/token",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /auth/token`

`HEAD /auth/token`


<!-- END_2befb2b1824b6f992a137039911e1a3d -->

<!-- START_b49beb5ea7965fcafa54f677e4d8d686 -->
## Returns the authenticated user

> Example request:

```bash
curl -X GET "http://farmhouse.dev//admin/me" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/me",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /admin/me`

`HEAD /admin/me`


<!-- END_b49beb5ea7965fcafa54f677e4d8d686 -->

#Brand Contact Us
<!-- START_331329fc0ce960909c5f0d7fdde458c7 -->
## Send a contact us enquiry

> Example request:

```bash
curl -X POST "http://farmhouse.dev/contacts" \
-H "Accept: application/json" \
    -d "name"="dolorem" \
    -d "email"="dolorem" \
    -d "message"="dolorem" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev/contacts",
    "method": "POST",
    "data": {
        "name": "dolorem",
        "email": "dolorem",
        "message": "dolorem"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST contacts`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | 
    email | string |  required  | 
    message | string |  required  | 

<!-- END_331329fc0ce960909c5f0d7fdde458c7 -->

#Brands
<!-- START_154fdf3f10907800fc49f22fe5b83ef3 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://farmhouse.dev//admin/brands" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/brands",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /admin/brands`

`HEAD /admin/brands`


<!-- END_154fdf3f10907800fc49f22fe5b83ef3 -->

<!-- START_91282c59edde9f59eead8b28a7a4a410 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://farmhouse.dev//admin/brands" \
-H "Accept: application/json" \
    -d "name"="est" \
    -d "email"="moriah11@example.net" \
    -d "address"="est" \
    -d "telephone"="est" \
    -d "logo"="est" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/brands",
    "method": "POST",
    "data": {
        "name": "est",
        "email": "moriah11@example.net",
        "address": "est",
        "telephone": "est",
        "logo": "est"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /admin/brands`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | 
    email | email |  required  | 
    address | string |  optional  | 
    telephone | string |  optional  | 
    logo | file |  optional  | Must be a file upload

<!-- END_91282c59edde9f59eead8b28a7a4a410 -->

<!-- START_04db8e6536d315f3a42af12b2b0e1a00 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://farmhouse.dev//admin/brands/{brand}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/brands/{brand}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /admin/brands/{brand}`

`HEAD /admin/brands/{brand}`


<!-- END_04db8e6536d315f3a42af12b2b0e1a00 -->

<!-- START_0afb10fabaa5fe2ffaca6fc3ea4acd9c -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://farmhouse.dev//admin/brands/{brand}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/brands/{brand}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT /admin/brands/{brand}`

`PATCH /admin/brands/{brand}`


<!-- END_0afb10fabaa5fe2ffaca6fc3ea4acd9c -->

<!-- START_980d20998f3ef2b62bc4c01efc2ff6c4 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://farmhouse.dev//admin/brands/{brand}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/brands/{brand}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE /admin/brands/{brand}`


<!-- END_980d20998f3ef2b62bc4c01efc2ff6c4 -->

#Events
Event management per brand
Class EventController
<!-- START_6adce0d2b276ccc1dbc21c8bdc2442b4 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://farmhouse.dev//admin/events" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/events",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /admin/events`

`HEAD /admin/events`


<!-- END_6adce0d2b276ccc1dbc21c8bdc2442b4 -->

<!-- START_efc9e2259908c5e1b61e5a89ef75a0f7 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://farmhouse.dev//admin/events" \
-H "Accept: application/json" \
    -d "name"="illo" \
    -d "event_date"="1972-08-06" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/events",
    "method": "POST",
    "data": {
        "name": "illo",
        "event_date": "1972-08-06"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /admin/events`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | 
    event_date | date |  required  | 

<!-- END_efc9e2259908c5e1b61e5a89ef75a0f7 -->

<!-- START_c298e4b1ae995f9c2fa3f98b0d2da1e9 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://farmhouse.dev//admin/events/{event}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/events/{event}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /admin/events/{event}`

`HEAD /admin/events/{event}`


<!-- END_c298e4b1ae995f9c2fa3f98b0d2da1e9 -->

<!-- START_c5574deeb47be1290229fda88e903ea5 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://farmhouse.dev//admin/events/{event}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/events/{event}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT /admin/events/{event}`

`PATCH /admin/events/{event}`


<!-- END_c5574deeb47be1290229fda88e903ea5 -->

<!-- START_b5c89506f59fbec073fc14b3b1f516f2 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://farmhouse.dev//admin/events/{event}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/events/{event}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE /admin/events/{event}`


<!-- END_b5c89506f59fbec073fc14b3b1f516f2 -->

#Products
Class ProductController
<!-- START_befe3a2f6f2e77c0a79dbe9021ad1344 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://farmhouse.dev//admin/products" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/products",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /admin/products`

`HEAD /admin/products`


<!-- END_befe3a2f6f2e77c0a79dbe9021ad1344 -->

<!-- START_cc73520ed6b439a0e09dd0d61864af57 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://farmhouse.dev//admin/products" \
-H "Accept: application/json" \
    -d "attribute_set_id"="corrupti" \
    -d "name"="corrupti" \
    -d "sku"="corrupti" \
    -d "price"="52764" \
    -d "cost"="52764" \
    -d "type_id"="simple" \
    -d "special_from"="Wednesday, 14-Jun-17 00:00:00 UTC" \
    -d "special_to"="Friday, 02-Jan-70 00:00:00 UTC" \
    -d "categories"="corrupti" \
    -d "images"="corrupti" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/products",
    "method": "POST",
    "data": {
        "attribute_set_id": "corrupti",
        "name": "corrupti",
        "sku": "corrupti",
        "price": 52764,
        "cost": 52764,
        "type_id": "simple",
        "special_from": "Wednesday, 14-Jun-17 00:00:00 UTC",
        "special_to": "Friday, 02-Jan-70 00:00:00 UTC",
        "categories": "corrupti",
        "images": "corrupti"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /admin/products`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    attribute_set_id | string |  required  | Valid attribute_set id
    name | string |  required  | 
    sku | string |  required  | 
    price | numeric |  required  | 
    cost | numeric |  optional  | 
    type_id | string |  optional  | `simple` or `configurable`
    special_from | date |  optional  | Must be a date after: `Tuesday, 13-Jun-17 00:00:00 UTC`
    special_to | date |  optional  | Must be a date after: `Thursday, 01-Jan-70 00:00:00 UTC`
    categories | array |  optional  | 
    images | array |  optional  | 

<!-- END_cc73520ed6b439a0e09dd0d61864af57 -->

<!-- START_57cf563c2f5b7481294fbb4c3795ce54 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://farmhouse.dev//admin/products/{product}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/products/{product}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /admin/products/{product}`

`HEAD /admin/products/{product}`


<!-- END_57cf563c2f5b7481294fbb4c3795ce54 -->

<!-- START_055105e401ceebb0ea2f8669d00f7dc9 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://farmhouse.dev//admin/products/{product}" \
-H "Accept: application/json" \
    -d "name"="itaque" \
    -d "sku"="itaque" \
    -d "price"="591518947" \
    -d "cost"="591518947" \
    -d "type_id"="configurable" \
    -d "special_from"="Wednesday, 14-Jun-17 00:00:00 UTC" \
    -d "special_to"="Friday, 02-Jan-70 00:00:00 UTC" \
    -d "categories"="itaque" \
    -d "images"="itaque" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/products/{product}",
    "method": "PUT",
    "data": {
        "name": "itaque",
        "sku": "itaque",
        "price": 591518947,
        "cost": 591518947,
        "type_id": "configurable",
        "special_from": "Wednesday, 14-Jun-17 00:00:00 UTC",
        "special_to": "Friday, 02-Jan-70 00:00:00 UTC",
        "categories": "itaque",
        "images": "itaque"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT /admin/products/{product}`

`PATCH /admin/products/{product}`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  optional  | 
    sku | string |  optional  | 
    price | numeric |  optional  | 
    cost | numeric |  optional  | 
    type_id | string |  optional  | `simple` or `configurable`
    special_from | date |  optional  | Must be a date after: `Tuesday, 13-Jun-17 00:00:00 UTC`
    special_to | date |  optional  | Must be a date after: `Thursday, 01-Jan-70 00:00:00 UTC`
    categories | array |  optional  | 
    images | array |  optional  | 

<!-- END_055105e401ceebb0ea2f8669d00f7dc9 -->

<!-- START_6d4d281618468e858f51780c4731c17d -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://farmhouse.dev//admin/products/{product}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/products/{product}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE /admin/products/{product}`


<!-- END_6d4d281618468e858f51780c4731c17d -->

#Products
Class ProductsController
<!-- START_79ca561b9eb7417f22d79d757d300755 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://farmhouse.dev//admin/category/{category}/products" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/category/{category}/products",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /admin/category/{category}/products`

`HEAD /admin/category/{category}/products`


<!-- END_79ca561b9eb7417f22d79d757d300755 -->

<!-- START_fd37d8084053e7a8ce6d23023db56ded -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://farmhouse.dev//admin/category/{category}/products" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/category/{category}/products",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /admin/category/{category}/products`


<!-- END_fd37d8084053e7a8ce6d23023db56ded -->

<!-- START_c918ab091bb245f3aa50f8f7ed76ef8e -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://farmhouse.dev//admin/category/{category}/products/{product}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/category/{category}/products/{product}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /admin/category/{category}/products/{product}`

`HEAD /admin/category/{category}/products/{product}`


<!-- END_c918ab091bb245f3aa50f8f7ed76ef8e -->

<!-- START_94aaf3195d9141ba47ebe9efdef157ee -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://farmhouse.dev//admin/category/{category}/products/{product}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/category/{category}/products/{product}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT /admin/category/{category}/products/{product}`

`PATCH /admin/category/{category}/products/{product}`


<!-- END_94aaf3195d9141ba47ebe9efdef157ee -->

<!-- START_56f5b83a0f9646a706463328aa58920c -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://farmhouse.dev//admin/category/{category}/products/{product}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/category/{category}/products/{product}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE /admin/category/{category}/products/{product}`


<!-- END_56f5b83a0f9646a706463328aa58920c -->

#Stores
<!-- START_f490d3ff26bbadeb40e48cb63f30c7cc -->
## List all of the brands stores

> Example request:

```bash
curl -X GET "http://farmhouse.dev//admin/stores" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/stores",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /admin/stores`

`HEAD /admin/stores`


<!-- END_f490d3ff26bbadeb40e48cb63f30c7cc -->

<!-- START_f5d67cb9d05186c79f4f9268484e3793 -->
## Create a store related to a brand

> Example request:

```bash
curl -X POST "http://farmhouse.dev//admin/stores" \
-H "Accept: application/json" \
    -d "name"="quia" \
    -d "address"="quia" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/stores",
    "method": "POST",
    "data": {
        "name": "quia",
        "address": "quia"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /admin/stores`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | 
    address | string |  required  | 

<!-- END_f5d67cb9d05186c79f4f9268484e3793 -->

<!-- START_30288a2773deef23c0fc38448ccbb9d6 -->
## Show a particular store

> Example request:

```bash
curl -X GET "http://farmhouse.dev//admin/stores/{store}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/stores/{store}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /admin/stores/{store}`

`HEAD /admin/stores/{store}`


<!-- END_30288a2773deef23c0fc38448ccbb9d6 -->

<!-- START_4f410cbb4754c0561af3a26eb713cd7c -->
## Update store

> Example request:

```bash
curl -X PUT "http://farmhouse.dev//admin/stores/{store}" \
-H "Accept: application/json" \
    -d "name"="omnis" \
    -d "address"="omnis" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/stores/{store}",
    "method": "PUT",
    "data": {
        "name": "omnis",
        "address": "omnis"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT /admin/stores/{store}`

`PATCH /admin/stores/{store}`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | 
    address | string |  required  | 

<!-- END_4f410cbb4754c0561af3a26eb713cd7c -->

<!-- START_04717666e80d6ebbb9af3f417d796661 -->
## Delete store

> Example request:

```bash
curl -X DELETE "http://farmhouse.dev//admin/stores/{store}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/stores/{store}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE /admin/stores/{store}`


<!-- END_04717666e80d6ebbb9af3f417d796661 -->

#general
<!-- START_a2cec50882cfcc8bf3c3e8aeda03bd6a -->
## Show the application dashboard.

> Example request:

```bash
curl -X GET "http://farmhouse.dev/" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev/",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "exception": null,
    "headers": {},
    "original": []
}
```

### HTTP Request
`GET `

`HEAD `


<!-- END_a2cec50882cfcc8bf3c3e8aeda03bd6a -->

<!-- START_516263046d575e1fb1c9ebc008521404 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://farmhouse.dev//pos/products" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//pos/products",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /pos/products`

`HEAD /pos/products`


<!-- END_516263046d575e1fb1c9ebc008521404 -->

<!-- START_74c799ae13eb350a1f7e669e2c060989 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://farmhouse.dev//pos/products" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//pos/products",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /pos/products`


<!-- END_74c799ae13eb350a1f7e669e2c060989 -->

<!-- START_e641d250a1b93ab3aadf773a894a8e87 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://farmhouse.dev//pos/products/{product}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//pos/products/{product}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /pos/products/{product}`

`HEAD /pos/products/{product}`


<!-- END_e641d250a1b93ab3aadf773a894a8e87 -->

<!-- START_42d66f04b8cc97aa2a012045bf1e79c5 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://farmhouse.dev//pos/products/{product}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//pos/products/{product}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT /pos/products/{product}`

`PATCH /pos/products/{product}`


<!-- END_42d66f04b8cc97aa2a012045bf1e79c5 -->

<!-- START_1e051b4b3786b1293fbbab4a145af96d -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://farmhouse.dev//pos/products/{product}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//pos/products/{product}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE /pos/products/{product}`


<!-- END_1e051b4b3786b1293fbbab4a145af96d -->

<!-- START_1005ee3e71a77a0f90906d03851d42c4 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://farmhouse.dev//admin/category" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/category",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /admin/category`

`HEAD /admin/category`


<!-- END_1005ee3e71a77a0f90906d03851d42c4 -->

<!-- START_561b3ab77c3edc32f06fa4a2d2168723 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://farmhouse.dev//admin/category" \
-H "Accept: application/json" \
    -d "name"="sunt" \
    -d "slug"="sunt" \
    -d "description"="sunt" \
    -d "parent_id"="sunt" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/category",
    "method": "POST",
    "data": {
        "name": "sunt",
        "slug": "sunt",
        "description": "sunt",
        "parent_id": "sunt"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /admin/category`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | 
    slug | string |  required  | Allowed: alpha-numeric characters, as well as dashes and underscores.
    description | string |  required  | 
    parent_id | string |  optional  | Valid category id

<!-- END_561b3ab77c3edc32f06fa4a2d2168723 -->

<!-- START_bf98fa520ed727ea7735cf27286a7941 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://farmhouse.dev//admin/category/{category}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/category/{category}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /admin/category/{category}`

`HEAD /admin/category/{category}`


<!-- END_bf98fa520ed727ea7735cf27286a7941 -->

<!-- START_c849d5188c510643c91694c8f9b124ff -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://farmhouse.dev//admin/category/{category}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/category/{category}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT /admin/category/{category}`

`PATCH /admin/category/{category}`


<!-- END_c849d5188c510643c91694c8f9b124ff -->

<!-- START_6094ddde21a6c3cb87e223e196268d33 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://farmhouse.dev//admin/category/{category}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://farmhouse.dev//admin/category/{category}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE /admin/category/{category}`


<!-- END_6094ddde21a6c3cb87e223e196268d33 -->


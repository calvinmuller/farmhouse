<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
  //

  protected $fillable = [
    'renderer',
    'frontend_input',
    'backend_type',
    'attribute_code',
    'attribute_name',
    'attribute_label',
    'is_configurable',
    'brand_id',
  ];


  protected $with = [
    'options'
  ];


  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
   */
  public function sets()
  {
    return $this->belongsToMany(AttributeSet::class);
  }


  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function brand()
  {
    return $this->belongsTo(Brand::class);
  }


  /**
   * Get attribute options
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function options()
  {
      return $this->hasMany(AttributeOption::class);
  }


  /**
   * Find an attribute by code
   * @param $attribute_code
   * @return mixed
   */
  public static function loadByCode($attribute_code)
  {
    return self::where('attribute_code', '=', $attribute_code)->firstOrFail();
  }
}

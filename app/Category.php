<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  //

  protected $fillable = ['name', 'slug', 'description', 'parent_id'];

  protected $table = 'categories';

  protected $hidden = [
    'objectable_id', 'objectable_type'
  ];

  protected $with = ['children'];

  /**
   * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
   */
  public function products()
  {
    return $this->morphedByMany(Product::class, 'category_object');
  }

  /**
   * We going to use slug for this call
   * @return string
   */
  public function getRouteKeyName()
  {
    return 'slug';
  }


  public function children()
  {
    return $this->hasOne(__CLASS__, 'parent_id');
  }
}

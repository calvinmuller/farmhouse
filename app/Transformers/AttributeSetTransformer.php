<?php
/**
 * Created by IntelliJ IDEA.
 * User: calvinmuller
 * Date: 2017/05/29
 * Time: 11:33 PM
 */

namespace App\Http\Transformers;


use App\AttributeSet;
use App\Store;
use League\Fractal\TransformerAbstract;

class AttributeSetTransformer extends TransformerAbstract
{

  /**
   * Transform store
   * @param Store $store
   * @return array
   */
  public function transform(AttributeSet $attributeSet)
  {
    return $attributeSet->toArray();
  }

}

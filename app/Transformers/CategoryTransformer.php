<?php
/**
 * Created by IntelliJ IDEA.
 * User: calvinmuller
 * Date: 2017/05/29
 * Time: 11:33 PM
 */

namespace App\Http\Transformers;


use App\Category;
use App\Product;
use App\Store;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{

  /**
   * Transform store
   * @param Store $store
   * @return array
   */
  public function transform(Category $category)
  {
    return $category->toArray();
  }

}

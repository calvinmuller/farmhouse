<?php
/**
 * Created by IntelliJ IDEA.
 * User: calvinmuller
 * Date: 2017/05/29
 * Time: 11:33 PM
 */

namespace App\Http\Transformers;


use App\Attribute;
use App\Store;
use League\Fractal\TransformerAbstract;

class AttributeTransformer extends TransformerAbstract
{

  /**
   * Transform store
   * @param Store $store
   * @return array
   */
  public function transform(Attribute $attribute)
  {
    return $attribute->toArray();
  }

}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: calvinmuller
 * Date: 2017/05/29
 * Time: 11:33 PM
 */

namespace App\Http\Transformers;


use App\Product;
use App\Store;
use League\Fractal\TransformerAbstract;

class ProductTransformer extends TransformerAbstract
{

  /**
   * Transform store
   * @param Store $store
   * @return array
   */
  public function transform(Product $product)
  {
    return array_merge($product->toArray(), [
      'images' => $product->images
    ]);
  }

}

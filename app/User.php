<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
  use Notifiable;

  protected $with = [
    'brand'
  ];

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name', 'email', 'password',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token',
    'brand_id'
  ];


  /**
   * @return \Illuminate\Database\Eloquent\Relations\HasOne
   */
  public function brand()
  {
    return $this->belongsTo(Brand::class);
  }
}

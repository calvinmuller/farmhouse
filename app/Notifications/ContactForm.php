<?php

namespace App\Notifications;

use App\Brand;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ContactForm extends Notification implements ShouldQueue
{
  use Queueable;

  /*  @var Brand $brand */
  private $brand;

  /*  @var array $data */
  private $data;

  /**
   * Create a new notification instance.
   *
   * @return void
   */
  public function __construct($data, $brand)
  {
    //
    $this->brand = $brand;
    $this->data = $data;
  }

  /**
   * Get the notification's delivery channels.
   *
   * @param  mixed $notifiable
   * @return array
   */
  public function via($notifiable)
  {
    return ['mail', 'database'];
  }

  /**
   * Get the mail representation of the notification.
   *
   * @param  mixed $notifiable
   * @return \Illuminate\Notifications\Messages\MailMessage
   */
  public function toMail($notifiable)
  {
    return (new MailMessage)->from($this->data['email'], $this->data['name'])
      ->markdown('contact', [
        'data' => $this->data,
        'brand' => $this->brand
      ]);
  }

  /**
   * Get the array representation of the notification.
   *
   * @param  mixed $notifiable
   * @return array
   */
  public function toArray($notifiable)
  {
    return $this->data;
  }
}

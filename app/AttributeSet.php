<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttributeSet extends Model
{
  //

  protected $fillable = [
    'name'
  ];


  /**
   * Get attributes
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function attributes()
  {
    return $this->belongsToMany(Attribute::class);
  }
}

<?php

namespace App\Http\Traits;

use Dingo\Api\Auth\Auth;

/**
 * @property \Dingo\Api\Dispatcher $api
 * @property \Illuminate\Auth\GenericUser|\Illuminate\Database\Eloquent\Model $user
 * @property \Dingo\Api\Auth\Auth $auth
 * @property \Dingo\Api\Http\Response\Factory $response
 */
trait BrandTrait
{

  /**
   * Get the authenticated users brand.
   *
   * @return mixed
   */
  protected function brand()
  {
    return app(Auth::class)->user()
      ->brand;
  }

}

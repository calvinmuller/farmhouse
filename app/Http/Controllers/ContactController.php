<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Http\Requests\ContactRequest;


/**
 * @resource Brand Contact Us
 * @Resource("contacts", uri="contacts")
 *
 */
class ContactController extends Controller
{

  /**
   * Send a contact us enquiry
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   *
   */
  public function store(ContactRequest $request)
  {
    /* @var Brand $brand */
    $brand = $request->brand;

    $result =  $brand->contact($request->except('brand'));

    return $result;
  }

}

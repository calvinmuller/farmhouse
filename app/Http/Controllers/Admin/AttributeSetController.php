<?php

namespace App\Http\Controllers\Admin;

use App\AttributeSet;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateAttributeSetRequest;
use App\Http\Transformers\AttributeSetTransformer;
use Illuminate\Http\Request;

/**
 * @resource Attribute Sets
 * Class AttributeSetController
 * @package App\Http\Controllers\Admin
 */
class AttributeSetController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //
    return $this->paginator(
      AttributeSet::with('attributes')->paginate(),
      new AttributeSetTransformer()
    );
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(CreateAttributeSetRequest $request)
  {
    //
    $attributeSet = AttributeSet::create($request->all());

    return $this->item($attributeSet, new AttributeSetTransformer());

  }

  /**
   * Display the specified resource.
   *
   * @param  \App\AttributeSet $attributeSet
   * @return \Illuminate\Http\Response
   */
  public function show(AttributeSet $attributeSet)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\AttributeSet $attributeSet
   * @return \Illuminate\Http\Response
   */
  public function edit(AttributeSet $attributeSet)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  \App\AttributeSet $attributeSet
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, AttributeSet $attributeSet)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\AttributeSet $attributeSet
   * @return \Illuminate\Http\Response
   */
  public function destroy(AttributeSet $attributeSet)
  {
    //
  }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Attribute;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateAttributeRequest;
use App\Http\Transformers\AttributeTransformer;
use Illuminate\Http\Request;

/**
 * @resource Attributes
 * Class AttributeController
 * @package App\Http\Controllers\Admin
 */
class AttributeController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //
    $attributes = Attribute::paginate();

    return $this->paginator($attributes, new AttributeTransformer());
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(CreateAttributeRequest $request)
  {
    //
    $attribute = Attribute::create($request->all());

    return $this->item($attribute, new AttributeTransformer());
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Attribute $attribute
   * @return \Illuminate\Http\Response
   */
  public function show(Attribute $attribute)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Attribute $attribute
   * @return \Illuminate\Http\Response
   */
  public function edit(Attribute $attribute)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  \App\Attribute $attribute
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Attribute $attribute)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Attribute $attribute
   * @return \Illuminate\Http\Response
   */
  public function destroy(Attribute $attribute)
  {
    //
  }
}

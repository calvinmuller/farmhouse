<?php

namespace App\Http\Controllers\Admin;

use App\Brand;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateStoreRequest;
use App\Http\Transformers\StoreTransformer;
use App\Store;
use Illuminate\Http\Request;

/**
 * @resource Stores
 * @Resource("Stores", uri="/stores")
 */
class StoreController extends Controller
{
  //

  /**
   * List all of the brands stores
   * @return \Dingo\Api\Http\Response
   */
  public function index()
  {

    $stores = $this->brand()->stores()
      ->paginate();

    return $this->response->paginator($stores, new StoreTransformer());
  }


  /**
   * Create a store related to a brand
   * @param CreateStoreRequest $request
   * @return mixed
   */
  public function store(CreateStoreRequest $request)
  {

    $store = $this->user
      ->brand
      ->stores()
      ->create($request->all());

    return $this->item($store, new StoreTransformer());

  }


  /**
   * Show a particular store
   * @param Store $store
   * @return mixed
   */
  public function show(Store $store)
  {

    return $this->item($store, new StoreTransformer());
  }


  /**
   * Update store
   * @param CreateStoreRequest $storeRequest
   * @param Store $store
   * @return mixed
   */
  public function update(CreateStoreRequest $storeRequest, Store $store)
  {

    $store->fill($storeRequest->all())
      ->save();

    return $this->item($store, new StoreTransformer());
  }


  /**
   * Delete store
   * @param Store $store
   * @return \Dingo\Api\Http\Response
   * @throws \Exception
   */
  public function destroy(Store $store)
  {
    $store->delete();

    return $this->response->noContent();
  }
}

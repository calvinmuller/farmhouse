<?php

namespace App\Http\Controllers\Admin;

use App\Brand;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateBrandRequest;
use App\Http\Transformers\BrandTransformer;
use Illuminate\Http\Request;

/**
 * @resource Brands
 * @Resource("Brands", uri="admin/brands")
 */
class BrandController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //

    $brands = Brand::paginate();

    return $this->paginator($brands, new BrandTransformer());
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(CreateBrandRequest $request)
  {
    //
    $brand = Brand::create($request->all());

    $brand = $brand->attachLogo($request->file('logo'));

    return $this->item($brand, new BrandTransformer());

  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Brand $brand
   * @return \Illuminate\Http\Response
   */
  public function show(Brand $brand)
  {
    //

    return $this->item($brand, new BrandTransformer());
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Brand $brand
   * @return \Illuminate\Http\Response
   */
  public function edit(Brand $brand)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  \App\Brand $brand
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Brand $brand)
  {
    //
    $brand->update($request->all());

    $brand->attachLogo($request->file('logo'));

    return $this->item($brand, new BrandTransformer());
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Brand $brand
   * @return \Illuminate\Http\Response
   */
  public function destroy(Brand $brand)
  {
    //

    $brand->delete();

    return $this->item($brand, new BrandTransformer());
  }
}

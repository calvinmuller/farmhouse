<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Http\Transformers\ProductTransformer;
use App\Product;
use Illuminate\Http\Request;

/**
 * @resource Products
 * Class ProductController
 * @package App\Http\Controllers
 */
class ProductController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $products = $this->brand()
      ->products()
      ->with('categories')
      ->paginate();
    //
    return $this->paginator($products, new ProductTransformer());
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(CreateProductRequest $request)
  {

    $product = $this->brand()
      ->products()
      ->create($request->all());

    $product->categories()
      ->sync($request->get('categories'));

    $product->attachImages($request->file('images'));

    return $this->item($product, new ProductTransformer());

  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Product $product
   * @return \Illuminate\Http\Response
   */
  public function show(Product $product)
  {
    //
    return $this->item($product, new ProductTransformer());
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Product $product
   * @return \Illuminate\Http\Response
   */
  public function edit(Product $product)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  \App\Product $product
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateProductRequest $request, Product $product)
  {

    //
    $product->fill($request->all())->save();

    $product->categories()
      ->sync($request->get('categories'));

    $product->attachImages($request->file('images'));

    return $this->item($product, new ProductTransformer());

  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Product $product
   * @return \Illuminate\Http\Response
   */
  public function destroy(Product $product)
  {
    //
    $product->delete();

    return $this->response->noContent();
  }
}

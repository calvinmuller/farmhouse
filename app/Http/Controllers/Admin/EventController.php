<?php

namespace App\Http\Controllers\Admin;

use App\Event;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateEventRequest;
use App\Http\Transformers\EventTransformer;
use Illuminate\Http\Request;

/**
 * @resource Events
 * Event management per brand
 * Class EventController
 * @package App\Http\Controllers
 */
class EventController extends Controller
{
  /**
   * Display a listing of the resource.
   * @transformer EventTransformer
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $events = $this->brand()->events()->paginate();
    //
    return $this->paginator($events, new EventTransformer());
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(CreateEventRequest $request)
  {
    //
    $event = $this->brand()->events()
      ->create($request->all());

//    $this->user->brand()->associate($event);
//    $this->brand()->associate($event);

    return $this->item($event, new EventTransformer());
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Event $event
   * @return \Illuminate\Http\Response
   */
  public function show(Event $event)
  {
    //
    return $this->item($event, new EventTransformer());
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Event $event
   * @return \Illuminate\Http\Response
   */
  public function edit(Event $event)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  \App\Event $event
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Event $event)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Event $event
   * @return \Illuminate\Http\Response
   */
  public function destroy(Event $event)
  {
    //
  }
}

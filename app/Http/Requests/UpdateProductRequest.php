<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;

class UpdateProductRequest extends BaseRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'name' => 'string',
      'sku' => 'unique:products,sku', //$this->product->id,
      'price' => 'numeric',
      'cost' => 'numeric',
      'type_id' => 'in:simple,configurable',
      'special_from' => 'date|after:today',
      'special_to' => 'date|after:special_from',
      'categories' => 'array',
      'images' => 'array'
      //
    ];
  }
}

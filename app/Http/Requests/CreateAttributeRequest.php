<?php

namespace App\Http\Requests;

class CreateAttributeRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'renderer' => '',
          'frontend_input' => 'required',
          'backend_type' => 'required',
          'attribute_code' => 'required',
          'attribute_name' => 'required',
          'attribute_label' => 'required',
          'is_configurable' => 'required|boolean'
        ];
    }
}

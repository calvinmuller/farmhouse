<?php

namespace App\Http\Requests;

class CreateProductRequest extends BaseRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'attribute_set_id' => 'required|exists:attribute_sets,id',
      'name' => 'required|string',
      'sku' => 'required|unique:products,sku',
      'price' => 'required|numeric',
      'cost' => 'numeric',
      'type_id' => 'in:simple,configurable',
      'special_from' => 'date|after:today',
      'special_to' => 'date|after:special_from',
      'categories' => 'array',
      'images' => 'array'
      //
    ];
  }
}

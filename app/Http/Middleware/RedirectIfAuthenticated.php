<?php

namespace App\Http\Middleware;

use App\Brand;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  \Closure $next
   * @param  string|null $guard
   * @return mixed
   */
  public function handle($request, Closure $next, $guard = null)
  {

    if (!$request->hasHeader('brand-id')) {
      abort(500, 'Invalid request header');
    }
    $brand = Brand::find($request->header('brand-id'));

    if (!$brand) {
      abort(404);
    }

    $request->merge(['brand' => $brand]);

    return $next($request);
  }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
  //

  protected $fillable = [
    'name',
    'path',
    'filename',
    'thumbnail',
    'order'
  ];

  protected $hidden = [
    'imageable_type', 'imageable_id'
  ];

  /**
   * Get all of the owning addressable models.
   */
  public function imageable()
  {
    return $this->morphTo();
  }

  /**
   * @param array $options
   */
  public function createMany(array $options = array())
  {
    print_r($options);
    exit();
  }
}

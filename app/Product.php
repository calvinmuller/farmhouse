<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  //

  protected $fillable = [
    'sku', 'name', 'brand', 'price',
    'supplier_id', 'type_id', 'special_from', 'special_to', 'cost',
    'attribute_set_id'
  ];


  protected $with = [
    'values'
  ];


  /**
   * Get the investments address
   * @return \Illuminate\Database\Eloquent\Relations\MorphMany
   */
  public function categories()
  {
    return $this->morphToMany(Category::class, 'category_object');
  }


  /**
   * Upload images if there are any
   * @param array $images
   * @return $this
   */
  public function attachImages($images = array())
  {
    $i = 0;

    if (count($images) == 0) {
      return $this;
    }

    foreach ($images as $uploadedImage) {
      $path = $uploadedImage->store($this->sku);

      $image = new Image([
        'path' => $path,
        'name' => $uploadedImage->getClientOriginalName(),
        'filename' => $uploadedImage->getClientOriginalName(),
        'thumbnail' => $path,
        'order' => $i++
      ]);

      $this->images()->save($image);

    }

    return $this;
  }


  /**
   *
   * Return images
   * @return \Illuminate\Database\Eloquent\Relations\MorphMany
   */
  public function images()
  {
    return $this->morphMany(Image::class, 'imageable')
      ->orderBy('order', 'ASC');
  }


  /**
   * Advanced saving here
   * @param array $options
   * @return bool
   */
  public function save(array $options = array())
  {

    return parent::save($options);
  }


  /**
   * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
   */
  public function values()
  {
    return $this->hasManyThrough(Attribute::class, AttributeValues::class);
  }

}

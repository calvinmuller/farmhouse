<?php

namespace App;

use App\Notifications\ContactForm;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Brand extends Model
{
  use Notifiable;
  //
  protected $fillable = ['name', 'address', 'telephone', 'email'];

  protected $with = ['logo'];

  /**
   * Get stores related to brand
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function stores()
  {
    return $this->hasMany(Store::class);
  }

  /**
   * Get users related to a brand
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function user()
  {
    return $this->hasMany(User::class);
  }


  /**
   * Get all events brand related
   * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
   */
  public function events()
  {
    return $this->belongsToMany(Event::class);
  }

  /**
   * Products related to a brand
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function products()
  {
    return $this->hasMany(Product::class);
  }

  /**
   * Upload images if there are any
   * @param array $images
   * @return $this
   */
  public function attachLogo($logo)
  {
    if (count($logo) == 0) {
      return $this;
    }

    $path = $logo->store($this->name, 's3');

    $image = new Image([
      'path' => $path,
      'name' => $logo->getClientOriginalName(),
      'filename' => $logo->getClientOriginalName(),
      'thumbnail' => $path,
      'order' => 0
    ]);

    $this->logo()->save($image);

    return $this;
  }


  /**
   *
   * Return images
   * @return \Illuminate\Database\Eloquent\Relations\MorphMany
   */
  public function logo()
  {
    return $this->morphOne(Image::class, 'imageable')
      ->orderBy('order', 'ASC');
  }


  /**
   * Send the contact notification
   * @param $data
   */
  public function contact($data)
  {

    return $this->notify(new ContactForm($data, $this));
  }

}

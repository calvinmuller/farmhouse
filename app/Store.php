<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
  //

  protected $fillable = ['name', 'logo', 'address'];


  public function brand()
  {
    return $this->hasOne(Brand::class);
  }
}

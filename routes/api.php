<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', ['namespace' => $this->namespace, 'middleware' => 'api'], function ($api) {
  $api->get('', 'HomeController@index');

  $api->group(['prefix' => 'auth'], function ($api) {
    $api->post('login', 'AuthenticateController@authenticate');
    $api->post('logout', 'AuthenticateController@logout');
    $api->get('token', 'AuthenticateController@getToken');
  });

  $api->group(['prefix' => 'pos', 'middleware' => ['api.auth']], function ($api) {
    $api->resource('products', 'ProductController');
  });

  $api->group(['prefix' => 'admin', 'middleware' => ['api.auth']], function ($api) {

    $api->get('me', 'AuthenticateController@me');
    $api->resource('brands', 'Admin\BrandController');
    $api->resource('stores', 'Admin\StoreController');
    $api->resource('events', 'Admin\EventController');
    $api->resource('products', 'Admin\ProductController');
    $api->resource('category', 'Admin\CategoryController');
    $api->resource('category.products', 'Admin\Category\ProductsController');
    $api->resource('attributes', 'Admin\AttributeController');
    $api->resource('attribute-sets', 'Admin\AttributeSetController');
  });

});
